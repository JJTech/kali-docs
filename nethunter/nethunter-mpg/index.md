---
title: NetHunter Metasploit Payload Generator
description:
icon:
date: 2019-11-29
type: post
weight: 300
author: ["re4son",]
tags: ["",]
keywords: ["",]
og_description:
---

The [MSFvenom Payload Creator (MFSPC)](https://github.com/g0tmi1k/msfpc) was written by g0tmi1k to take the pain out of generating payloads using the Metasploit msfvenom utility. Simply select your payload, set its options, and generate your payload.

![](./nethunter-mpc.png)
